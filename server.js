//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');

var movimientosJSON = require('./movimientosv2.json');

var bodyParser = require('body-parser');
app.use(bodyParser.json());

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get('/', function(req, res) {
  res.sendFile(path.join(__dirname, 'index.html'));
});

app.post('/', function(req,res) {
  res.send('Hola recibido su petición post cambiada');
});

app.put('/', function(req, res) {
  res.send('Hola recibido su petición put');
});

app.delete('/', function(req,res) {
  res.send('Hola recibido su petición delete');
});

app.get('/Movimientos', function(req, res) {
  res.sendfile('movimientosv1.json');
});

app.get('/v2/Movimientos', function(req, res) {
  res.json(movimientosJSON);
});

app.get('/v2/Movimientos/:id', function(req, res) {
  console.log('req.params.id:', req.params.id);
  byId = movimientosJSON.find(el => el.id == req.params.id);
  res.json(byId);
});

app.get('/v2/Movimientosq', function(req, res) {
  console.log('req.query.id:', req.query.id);
  res.send('Query recibido ' + req.query.id);
});

app.post('/v2/Movimientos', function(req, res) {
  var nuevo = req.body;
  var idnuevo = movimientosJSON.length + 1;
  nuevo.id = idnuevo;
  movimientosJSON.push(nuevo);
  res.send('Movimiento dado de alta');
})

app.get('/Clientes/:idcliente', function(req, res) {
  res.send('Aquí tiene al cliente número ' + req.params.idcliente);
});
